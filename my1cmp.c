/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <ctype.h>
/*----------------------------------------------------------------------------*/
#define PATH_SEP '/'
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	char* basename(char* pname);
	FILE* openfile(char* fname);
	int filesize(FILE* pfile);
	FILE *pfile1, *pfile2;
	int size1, size2, count, diffc;
	/* check argument */
	if (argc<3)
	{
		printf("\nCompare files and shows byte difference.\n\n");
		printf("Usage: %s <filename1> <filename2>\n\n",basename(argv[0]));
		return 0;
	}
	/* try to open files */
	pfile1 = openfile(argv[1]);
	pfile2 = openfile(argv[2]);
	if (!pfile1||!pfile2)
	{
		if (pfile1) fclose(pfile1);
		if (pfile2) fclose(pfile2);
		return 1;
	}
	/* get file size */
	size1 = filesize(pfile1);
	size2 = filesize(pfile2);
	printf("\nFile 1: %s [%d byte(s)]\n",basename(argv[1]),size1);
	printf("File 2: %s [%d byte(s)]\n\n",basename(argv[2]),size2);
	count = 0; diffc = 0;
	while(1)
	{
		int test1 = fgetc(pfile1);
		int test2 = fgetc(pfile2);
		if(test1==EOF||test2==EOF)
			break;
		if(test1!=test2)
		{
			printf("Location: %08X - File1: %02X File2: %02X\n",
				count,test1,test2);
			diffc++;
			if(diffc%20==0)
			{
				char check[40];
				printf("\n[1] Continue [2] Exit --> Choice[1]: ");
				fgets(check,40,stdin);
				if(check[0]=='2') break;
				printf("\nFile 1: %s [%d byte(s)]\n",basename(argv[1]),size1);
				printf("File 2: %s [%d byte(s)]\n\n",basename(argv[2]),size2);
			}
		}
		count++;
	}
	if(diffc==0)
	{
		if(size1==size2) printf("\nFiles are exact match!\n");
		else printf("\nFiles are the same up till %d byte(s)\n",count);
	}
	putchar('\n');
	fclose(pfile1);
	fclose(pfile2);
	return 0;
}
/*----------------------------------------------------------------------------*/
char* basename(char* pname)
{
	int index = 0, count = 0;
	while (pname[count]!='\0')
	{
		if(pname[count]==PATH_SEP)
			index = count + 1;
		count++;
	}
	if (index==count) index = 0;
	return &pname[index];
}
/*----------------------------------------------------------------------------*/
FILE* openfile(char* fname)
{
	FILE *pfile = fopen(fname,"rb+");
	if (!pfile) printf("Cannot open file '%s'!\n",fname);
	return pfile;
}
/*----------------------------------------------------------------------------*/
int filesize(FILE* pfile)
{
	int size;
	fseek(pfile,0L,SEEK_END);
	size = ftell(pfile);
	fseek(pfile,0L,SEEK_SET);
	return size;
}
/*----------------------------------------------------------------------------*/
