# A simple makefile for my1cmp

CC  = gcc
RM = rm -f

PROGRAM = my1cmp
OBJECTS = $(PROGRAM).o

CFLAG = -c -Wall
LFLAG = -s

all: ${PROGRAM}

${PROGRAM}: $(OBJECTS)
	${CC} ${LFLAG} -o $@ $^

.c.o:
	${CC} ${CFLAG} -o $@ $<

clean:
	-$(RM) ${PROGRAM} *.o *.obj *.exe
